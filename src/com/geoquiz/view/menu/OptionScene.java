package com.geoquiz.view.menu;

import com.geoquiz.view.utility.Background;

import java.io.IOException;

import com.geoquiz.view.enums.MenuButtons;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonBuilder;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelBuilder;

import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The scene where user can change game options.
 */
class OptionScene extends GameScene {

    private static final double POS_2_X = 250;
    private static final double POS_O = 275;
    private static final double POS = 575;
    private static final double POS_1_X = 100;
    private static final double USER_LABEL_FONT = 40;

    private final CheckBox sound = new CheckBox("MUSICA");
    private final CheckBox effect = new CheckBox("EFFETTI SONORI");

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public OptionScene(final Stage mainStage) {
        super(mainStage);

        final MyLabel userLabel = new MyLabelBuilder().setName("USER: " + LoginMenuScene.getUsername())
                                                      .setFont(USER_LABEL_FONT)
                                                      .build();

        final MyButton back;
        final MyButton save;
        back = new MyButtonBuilder().createMyButton();
        save = new MyButtonBuilder().setName(MenuButtons.SAVE.toString())
                                    .createMyButton();

        sound.setSelected(!MainWindow.isMusicDisabled());
        sound.setStyle("-fx-font-size: 35");
        effect.setSelected(MainWindow.isWavEnabled());
        effect.setStyle("-fx-font-size: 35");

        VBox vbox = new VBox(sound, effect);
        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_O);
        VBox vbox2 = new VBox();
        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS);
        vbox2.getChildren().addAll((Node) save, (Node) back);

        ((Node) back).setOnMouseClicked(event -> {
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        ((Node) save).setOnMouseClicked(event -> this.save());

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox, vbox2,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(panel);

    }

    private void save() {
        if (!sound.isSelected()) {
            MainWindow.disableMusic();
        } else {
            MainWindow.resumeMusic();
        }
        if (!effect.isSelected()) {
            MainWindow.stopClick();
        } else {
            MainWindow.playClick();
        }
    }

}
