package com.geoquiz.view.menu;

import com.geoquiz.view.button.*;
import com.geoquiz.view.enums.ButtonsCategory;
import com.geoquiz.view.utility.Background;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelBuilder;

import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The scene where user can choose game category.
 */
class CategoryScene extends GameScene {

    private static final double CONTAINER_1_X_POSITION = 100;
    private static final double CONTAINER_1_Y_POSITION = 450;
    private static final double CONTAINER_2_X_POSITION = 250;
    private static final double CONTAINER_2_Y_POSITION = 300;
    private static final double BACK_CONTAINER_X_POSITION = 450;
    private static final double BACK_CONTAINER_Y_POSITION = 600;
    private static final int CONTAINER_SPACING = 10;
    private static final String USER_NAME_TAG = "USER: ";
    private static final double USER_LABEL_FONT = 40;

    private final HBox categoryButtonContainer1 = new HBox(CONTAINER_SPACING);
    private final HBox categoryButtonContainer2 = new HBox(CONTAINER_SPACING);

    private final OptionsSelected optionsSelected;
    private final List<MyButton> buttonsList;

    /**
     * @param mainStage the stage where the scene is called.
     */
    public CategoryScene(final Stage mainStage, OptionsSelected optionsSelected) {
        super(mainStage);

        final MyButton back;

        final MyLabel userLabel = new MyLabelBuilder().setName(USER_NAME_TAG + LoginMenuScene.getUsername())
                                                      .setFont(USER_LABEL_FONT)
                                                      .build();

        this.optionsSelected = optionsSelected;

        buttonsList = new ArrayList<>();

        initializeCategoryButtons(ButtonsCategory.MONUMENTS);
        initializeCategoryButtons(ButtonsCategory.CAPITALS);
        initializeCategoryButtons(ButtonsCategory.FLAGS);
        initializeCategoryButtons(ButtonsCategory.CURRENCIES);
        initializeCategoryButtons(ButtonsCategory.DISHES);

        back = new MyButtonBuilder().createMyButton();

        categoryButtonContainer1.setTranslateX(CONTAINER_1_X_POSITION);
        categoryButtonContainer1.setTranslateY(CONTAINER_1_Y_POSITION);

        categoryButtonContainer2.setTranslateX(CONTAINER_2_X_POSITION);
        categoryButtonContainer2.setTranslateY(CONTAINER_2_Y_POSITION);

        VBox backButtonContainer = new VBox();
        backButtonContainer.setTranslateX(BACK_CONTAINER_X_POSITION);
        backButtonContainer.setTranslateY(BACK_CONTAINER_Y_POSITION);


        addButtonsToContainers();
        backButtonContainer.getChildren().add((Node) back);

        ((Node) back).setOnMouseClicked(event -> {
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        initializeCategoryButtons(mainStage);

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), categoryButtonContainer1, categoryButtonContainer2, backButtonContainer,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(panel);
    }

    private void addButtonsToContainers() {
        for (int i = 0; i < buttonsList.size(); i++) {
            HBox c;
            if (i < buttonsList.size() / 2) {
                c = categoryButtonContainer2;
            } else {
                c = categoryButtonContainer1;
            }
            addButtonsToContainer(c, buttonsList.get(i));
        }
    }

    private void addButtonsToContainer(HBox container, MyButton button){
        container.getChildren().add((Node) button);
    }

    private void initializeCategoryButtons(ButtonsCategory categoryButtonName) {
        MyButton button = new MyButtonBuilder().setCategory(categoryButtonName)
                                               .createMyButton();
        buttonsList.add(button);
    }

    private void initializeCategoryButtons(Stage mainStage) {
        for (MyButton b : buttonsList) {
            ((Node) b).setOnMouseClicked(event -> {
                optionsSelected.setCategory((ButtonsCategory) b.getCategory());
                mainStage.setScene(new ModeScene(mainStage, optionsSelected));
            });
        }
    }
}
