package com.geoquiz.view.enums;

public enum Mode {
    /**
     * Represents the game mode "Classica".
     */
    CLASSICA,
    /**
     * Represents the game mode "Sfida".
     */
    SFIDA,
    /**
     * Represents the game mode "Allenamento".
     */
    ALLENAMENTO
}
