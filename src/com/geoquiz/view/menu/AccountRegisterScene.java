package com.geoquiz.view.menu;

import com.geoquiz.view.button.MyButtonBuilder;
import com.geoquiz.view.label.MyLabelBuilder;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.utility.ConfirmBox;
import com.geoquiz.view.enums.MenuButtons;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.enums.Labels;
import com.geoquiz.view.label.MyLabel;

import java.io.IOException;

import com.geoquiz.controller.account.Account;
import com.geoquiz.controller.account.AccountImpl;
import com.geoquiz.utility.Pair;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * The scene where the user can create a new account.
 */
class AccountRegisterScene extends GameScene {

    private static final double TF_OPACITY = 0.7;
    private static final double TF_FONT = 25;
    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 525;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_3_X = 450;
    private static final double POS_3_Y = 300;
    private static final double FONT = 35;

    private final TextField tfUser = new TextField();
    private final PasswordField tfPass = new PasswordField();
    private boolean okLogin;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     * @throws IOException
     *             for exception.
     */
    public AccountRegisterScene(final Stage mainStage) throws IOException {
        super(mainStage);

        final MyLabel username;
        final MyLabel password;

        final MyButton save;
        final MyButton back;

        final Account a = new AccountImpl("account.txt");

        save = new MyButtonBuilder().setName(MenuButtons.SAVE.toString())
                                    .createMyButton();
        back = new MyButtonBuilder().createMyButton();

        MyLabelBuilder label = new MyLabelBuilder().setColor(Color.WHITE)
                                                   .setFont(FONT);
        username = label.setName(Labels.USERNAME.toString())
                        .build();
        password = label.setName(Labels.PASSWORD.toString())
                        .build();

        tfUser.getFont();
        tfUser.setFont(Font.font(TF_FONT));
        tfUser.setOpacity(TF_OPACITY);
        tfUser.setPromptText("Username");

        tfPass.getFont();
        tfPass.setFont(Font.font(TF_FONT));
        tfPass.setOpacity(TF_OPACITY);
        tfPass.setPromptText("Password");

        VBox vbox1 = new VBox();
        vbox1.setTranslateX(POS_1_X);
        vbox1.setTranslateY(POS_1_Y);
        VBox vbox2 = new VBox(20);
        vbox2.setTranslateX(POS_2_X);
        vbox2.setTranslateY(POS_2_Y);
        VBox vbox3 = new VBox(10);
        vbox3.setTranslateX(POS_3_X);
        vbox3.setTranslateY(POS_3_Y);

        vbox1.getChildren().addAll((Node) save, (Node) back);
        vbox2.getChildren().addAll((Node) username, (Node) password);
        vbox3.getChildren().addAll(tfUser, tfPass);

        ((Node) back).setOnMouseClicked(event -> {
            try {
                mainStage.setScene(new LoginMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        ((Node) save).setOnMouseClicked(event -> {
            createUser();
            createPass();
            try {
                if (tfUser.getText().isEmpty() || tfPass.getText().isEmpty()) {
                    final Alert alert = ConfirmBox.getAlert("Username o password mancanti!", Color.BLACK);
                    alert.show();
                } else if (tfUser.getText().contains(" ") || tfPass.getText().contains(" ")) {
                    final Alert alert = ConfirmBox.getAlert("Errore! Gli spazi non sono consentiti!", Color.BLACK);
                    alert.show();
                } else {

                    try {
                        okLogin = true;
                        a.register(getUserAccountCreation());
                    } catch (IllegalStateException e) {
                        okLogin = false;
                        final Alert alert = ConfirmBox.getAlert("Errore! Username gia' esistente!", Color.BLACK);
                        alert.show();
                        e.printStackTrace();
                    }

                }
                if (okLogin) {
                    mainStage.setScene(new LoginMenuScene(mainStage));
                }
            } catch (IOException e) {

                e.printStackTrace();
            }
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox1, vbox2, vbox3,
                Background.getLogo());
        this.setRoot(panel);
    }

    /**
     * @return username.
     */
    private String createUser() {
        return tfUser.getText();
    }

    /**
     * @return password.
     */
    private String createPass() {
        return tfPass.getText();
    }

    /**
     * @return Pair (username, password) for account register.
     */
    private Pair<String, String> getUserAccountCreation() {
        return new Pair<>(tfUser.getText(), tfPass.getText());
    }

}
