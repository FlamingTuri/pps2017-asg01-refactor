package com.geoquiz.view.menu;

import com.geoquiz.controller.ranking.Ranking;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonBuilder;
import com.geoquiz.view.enums.ButtonsCategory;
import com.geoquiz.view.enums.Difficulty;
import com.geoquiz.view.enums.Mode;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelBuilder;
import com.geoquiz.view.utility.Background;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;

abstract class AbstractRankingScene extends GameScene {

    private static final double POS_1_X = 20;
    private static final double POS_Y_BACK = 650;
    private static final double FONT_MODE = 25;
    private static final double POS_X_CATEGORY_BOX = 50;
    private static final double POS_Y_CATEGORY_BOX = 200;
    private static final double POS_X_CATEGORY_BOX_2 = 650;
    private static final double POS_Y_CATEGORY_BOX_2 = 75;
    private static final double POS_X_CAPITALS_BOX = 300;
    private static final double POS_Y_CAPITALS_BOX = 205;
    private static final double POS_X_MONUMENTS_BOX = 300;
    private static final double POS_Y_MONUMENTS_BOX = 450;
    private static final double POS_X_FLAGS_BOX = 850;
    private static final double POS_Y_FLAGS_BOX = 80;
    private static final double POS_X_CURRENCIES_BOX = 850;
    private static final double POS_Y_CURRENCIES_BOX = 325;
    private static final double POS_X_DISHES_BOX = 850;
    private static final double POS_Y_DISHES_BOX = 570;
    private static final double TITLE_FONT = 95;
    private static final String SEPARATOR = " -> ";

    private final MyLabel title;

    private final Ranking ranking = Ranking.getInstance();

    AbstractRankingScene(final Stage mainStage){
        super(mainStage);

        final MyLabel capitals;
        final MyLabel monuments;
        final MyLabel flags;
        final MyLabel currencies;
        final MyLabel dishes;

        final MyButton back;

        title = new MyLabelBuilder()
                .setFont(TITLE_FONT)
                .build();

        initMap();

        back = new MyButtonBuilder().createMyButton();

        capitals = createModeLabel(ButtonsCategory.CAPITALS);
        monuments = createModeLabel(ButtonsCategory.MONUMENTS);
        flags = createModeLabel(ButtonsCategory.FLAGS);
        currencies = createModeLabel(ButtonsCategory.CURRENCIES);
        dishes = createModeLabel(ButtonsCategory.DISHES);

        MyLabel capitalsEasy = createStatsLabel(Difficulty.EASY, ButtonsCategory.CAPITALS);
        MyLabel capitalsMedium = createStatsLabel(Difficulty.MEDIUM, ButtonsCategory.CAPITALS);
        MyLabel capitalsHard = createStatsLabel(Difficulty.HARD, ButtonsCategory.CAPITALS);
        MyLabel capitalsChallenge = createStatsLabel(Mode.SFIDA, ButtonsCategory.CAPITALS);
        MyLabel monumentsEasy = createStatsLabel(Difficulty.EASY, ButtonsCategory.MONUMENTS);
        MyLabel monumentsMedium = createStatsLabel(Difficulty.MEDIUM, ButtonsCategory.MONUMENTS);
        MyLabel monumentsHard = createStatsLabel(Difficulty.HARD, ButtonsCategory.MONUMENTS);
        MyLabel monumentsChallenge = createStatsLabel(Mode.SFIDA, ButtonsCategory.MONUMENTS);
        MyLabel flagsClassic = createStatsLabel(Mode.CLASSICA, ButtonsCategory.FLAGS);
        MyLabel flagsChallenge = createStatsLabel(Mode.SFIDA, ButtonsCategory.FLAGS);
        MyLabel currenciesClassic = createStatsLabel(Mode.CLASSICA, ButtonsCategory.CURRENCIES);
        MyLabel currenciesChallenge = createStatsLabel(Mode.SFIDA, ButtonsCategory.FLAGS);
        MyLabel dishesClassic = createStatsLabel(Mode.CLASSICA, ButtonsCategory.DISHES);
        MyLabel dishesChallenge = createStatsLabel(Mode.SFIDA, ButtonsCategory.DISHES);

        VBox titleBox = new VBox();
        titleBox.getChildren().add((Node) title);

        VBox capitalsBox = new VBox();
        capitalsBox.getChildren().addAll((Node) capitalsEasy, (Node) capitalsMedium, (Node) capitalsHard,
                (Node) capitalsChallenge);

        VBox monumentsBox = new VBox();
        monumentsBox.getChildren().addAll((Node) monumentsEasy, (Node) monumentsMedium, (Node) monumentsHard,
                (Node) monumentsChallenge);

        VBox currenciesBox = new VBox();
        currenciesBox.getChildren().addAll((Node) currenciesClassic, (Node) currenciesChallenge);

        VBox flagsBox = new VBox();
        flagsBox.getChildren().addAll((Node) flagsClassic, (Node) flagsChallenge);

        VBox dishesBox = new VBox();
        dishesBox.getChildren().addAll((Node) dishesClassic, (Node) dishesChallenge);

        VBox categoryBox = new VBox(200);
        categoryBox.getChildren().addAll((Node) capitals, (Node) monuments);
        VBox categoryBox2 = new VBox(200);
        categoryBox2.getChildren().addAll((Node) flags, (Node) currencies, (Node) dishes);

        categoryBox.setTranslateX(POS_X_CATEGORY_BOX);
        categoryBox.setTranslateY(POS_Y_CATEGORY_BOX);
        categoryBox2.setTranslateX(POS_X_CATEGORY_BOX_2);
        categoryBox2.setTranslateY(POS_Y_CATEGORY_BOX_2);
        capitalsBox.setTranslateX(POS_X_CAPITALS_BOX);
        capitalsBox.setTranslateY(POS_Y_CAPITALS_BOX);
        monumentsBox.setTranslateX(POS_X_MONUMENTS_BOX);
        monumentsBox.setTranslateY(POS_Y_MONUMENTS_BOX);
        dishesBox.setTranslateX(POS_X_DISHES_BOX);
        dishesBox.setTranslateY(POS_Y_DISHES_BOX);
        flagsBox.setTranslateX(POS_X_FLAGS_BOX);
        flagsBox.setTranslateY(POS_Y_FLAGS_BOX);
        currenciesBox.setTranslateX(POS_X_CURRENCIES_BOX);
        currenciesBox.setTranslateY(POS_Y_CURRENCIES_BOX);

        VBox vbox = new VBox();
        vbox.setTranslateX(POS_1_X);
        vbox.setTranslateY(POS_Y_BACK);
        vbox.getChildren().add((Node) back);

        ((Node) back).setOnMouseClicked(event -> {
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox, categoryBox,
                categoryBox2, capitalsBox, monumentsBox, flagsBox, currenciesBox, dishesBox, titleBox);

        this.setRoot(panel);
    }

    protected abstract void initMap();

    protected abstract String getRecordByCategory(final String category, final String difficulty);

    private MyLabel createModeLabel(ButtonsCategory categoryName) {
        return new MyLabelBuilder().setName("" + categoryName)
                .setColor(Color.RED)
                .build();
    }

    private MyLabel createStatsLabel(Enum difficulty, ButtonsCategory categoryName) {
        return new MyLabelBuilder().setName(difficulty + SEPARATOR
                + this.getRecordByCategory(categoryName.toString(), difficulty.toString()))
                .setFont(FONT_MODE)
                .build();
    }

    /**
     * gets the value of title label.
     *
     * @return title label.
     */
    MyLabel getTitle() {
        return title;
    }

    /**
     * gets the controller.
     *
     * @return controller.
     */
    Ranking getRanking() {
        return ranking;
    }

}
