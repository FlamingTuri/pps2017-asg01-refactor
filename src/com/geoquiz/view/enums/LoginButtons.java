package com.geoquiz.view.enums;

public enum LoginButtons {
    /**
     * Log in if user is authenticated.
     */
    LOGIN,
    /**
     * Possibility to create an account.
     */
    REGISTER,
    /**
     * Exit the game.
     */
    EXIT
}
