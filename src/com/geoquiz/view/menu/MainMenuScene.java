package com.geoquiz.view.menu;

import java.io.IOException;

import com.geoquiz.controller.account.Account;
import com.geoquiz.controller.account.AccountImpl;
import com.geoquiz.view.enums.MenuButtons;
import com.geoquiz.view.button.MyButtonBuilder;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelBuilder;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.button.MyButton;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The scene where user can choose how to do.
 */
class MainMenuScene extends GameScene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double USER_LABEL_FONT = 40;
    private static final double POS_X_INSTRUCTIONS = 900;
    private static final double POS_Y_INSTRUCTIONS = 638;

    private OptionsSelected optionsSelected;

    /**
     * @param mainStage the stage where the scene is called.
     * @throws IOException for exception.
     */
    public MainMenuScene(final Stage mainStage) throws IOException {
        super(mainStage);

        final MyButton play;
        final MyButton ranking;
        final MyButton options;
        final MyButton back;
        final MyButton stats;
        final MyButton instructions;
        final Account a = new AccountImpl("account.txt");

        final MyLabel userLabel = new MyLabelBuilder().setName("USER: " + LoginMenuScene.getUsername())
                                                      .setFont(USER_LABEL_FONT)
                                                      .build();

        optionsSelected = new OptionsSelected();

        play = new MyButtonBuilder().setName(MenuButtons.PLAY.toString())
                .createMyButton();
        ranking = new MyButtonBuilder().setName(MenuButtons.RANKING.toString())
                .createMyButton();
        options = new MyButtonBuilder().setName(MenuButtons.OPTIONS.toString())
                .createMyButton();
        back = new MyButtonBuilder().setName(MenuButtons.BACK.toString())
                .createMyButton();
        stats = new MyButtonBuilder().setName(MenuButtons.STATS.toString())
                .createMyButton();
        instructions = new MyButtonBuilder().setName(MenuButtons.INSTRUCTIONS.toString())
                .createMyButton();

        VBox instructionsButtonBox = new VBox();
        instructionsButtonBox.setTranslateX(POS_X_INSTRUCTIONS);
        instructionsButtonBox.setTranslateY(POS_Y_INSTRUCTIONS);
        instructionsButtonBox.getChildren().add((Node) instructions);

        VBox vbox = new VBox();
        vbox.setTranslateX(POS_1_X);
        vbox.setTranslateY(POS_1_Y);
        vbox.getChildren().addAll((Node) play, (Node) stats, (Node) ranking, (Node) options, (Node) back);

        ((Node) back).setOnMouseClicked(event -> {
            a.logout();
            try {
                mainStage.setScene(new LoginMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        ((Node) instructions).setOnMouseClicked(event -> mainStage.setScene(new InstructionScene(mainStage)));

        ((Node) stats).setOnMouseClicked(event -> mainStage.setScene(new MyRankingScene(mainStage)));

        ((Node) ranking).setOnMouseClicked(event -> mainStage.setScene(new AbsoluteRankingScene(mainStage)));

        ((Node) options).setOnMouseClicked(event -> mainStage.setScene(new OptionScene(mainStage)));

        ((Node) play).setOnMouseClicked(event -> mainStage.setScene(new CategoryScene(mainStage, optionsSelected)));

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox,
                Background.getLogo(), (Node) userLabel, instructionsButtonBox);

        this.setRoot(panel);
    }

}
