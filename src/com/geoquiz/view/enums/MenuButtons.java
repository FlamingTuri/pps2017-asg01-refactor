package com.geoquiz.view.enums;

/**
 * The menu's buttons who a player can press.
 */
public enum MenuButtons {
    /**
     * Go to scene before.
     */
    BACK,
    /**
     * Enter in category scene.
     */
    PLAY,
    /**
     * Enter in option scene.
     */
    OPTIONS,
    /**
     * Register a new accont.
     */
    SAVE,
    /**
     * Enter in ranking scene.
     */
    RANKING,
    /**
     * Enter in statistics scene.
     */
    STATS,

    /**
     * Enter in instructions scene.
     */
    INSTRUCTIONS

}
