package com.geoquiz.view.menu;

import javax.xml.bind.JAXBException;

import com.geoquiz.view.enums.Difficulty;
import com.geoquiz.view.button.MyButtonBuilder;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelBuilder;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.button.MyButton;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * The scene where user can choose difficulty level.
 */
class LevelScene extends GameScene {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double USER_LABEL_FONT = 40;

    private final Stage mainStage;
    private final OptionsSelected optionsSelected;

    /**
     * @param mainStage the stage where the scene is called.
     */
    public LevelScene(final Stage mainStage, OptionsSelected optionsSelected) {
        super(mainStage);

        this.mainStage = mainStage;
        this.optionsSelected = optionsSelected;

        final MyLabel userLabel = new MyLabelBuilder().setName("USER: " + LoginMenuScene.getUsername())
                .setColor(Color.BLACK)
                .setFont(USER_LABEL_FONT)
                .build();

        final MyButton back = new MyButtonBuilder().createMyButton();

        ((Node) back).setOnMouseClicked(event -> mainStage.setScene(new CategoryScene(mainStage, optionsSelected)));

        MyButton easy = initializeDifficultyButton(Difficulty.EASY);
        MyButton medium = initializeDifficultyButton(Difficulty.MEDIUM);
        MyButton hard = initializeDifficultyButton(Difficulty.HARD);

        VBox vbox = new VBox();
        vbox.getChildren().addAll((Node) easy, (Node) medium, (Node) hard);
        VBox backVBox = new VBox();
        backVBox.getChildren().add((Node) back);

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);

        backVBox.setTranslateX(POS_1_X);
        backVBox.setTranslateY(POS_Y_BACK);

        Pane panel = new Pane();
        panel.getChildren().addAll(ModeScene.createBackgroundImage(), Background.createBackground(), vbox, backVBox,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(panel);

    }

    private MyButton initializeDifficultyButton(Difficulty difficulty) {
        MyButton button = new MyButtonBuilder().setName(difficulty.toString())
                .createMyButton();
        ((Node) button).setOnMouseClicked(event -> {
            try {
                optionsSelected.setDifficulty(difficulty);
                mainStage.setScene(new QuizGamePlay(mainStage, optionsSelected));
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        });
        return button;
    }
}
