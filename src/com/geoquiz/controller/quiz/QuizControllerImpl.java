package com.geoquiz.controller.quiz;

import java.io.InputStream;
import java.util.Optional;
import java.util.Set;

import javax.xml.bind.JAXBException;

import com.geoquiz.model.quiz.BasicMode;
import com.geoquiz.model.quiz.ExtendedMode;
import com.geoquiz.model.quiz.Quiz;
import com.geoquiz.model.quiz.QuizFactory;
import com.geoquiz.utility.ResourceLoader;
import com.geoquiz.view.enums.ButtonsCategory;
import com.geoquiz.view.enums.Difficulty;
import com.geoquiz.view.enums.Mode;

/**
 * Implementation of QuizController interface.
 */
public class QuizControllerImpl implements QuizController {

    private static final double FREEZE_TIME = 3000;

    private Quiz quiz;
    private ExtendedMode difficult;
    private BasicMode basicMode;

    /**
     * @param type       the type of the quiz.
     * @param mode       the mode of the quiz.
     * @param difficulty the difficulty of the quiz.
     * @throws JAXBException exception.
     */
    public QuizControllerImpl(final ButtonsCategory type, final Mode mode, final Optional<Difficulty> difficulty)
            throws JAXBException {

        difficult = null;
        basicMode = null;
        switch (mode){
            case CLASSICA:
                if(difficulty.isPresent()) {
                    switch (difficulty.get()) {
                        case EASY:
                            difficult = ExtendedMode.EASY;
                            break;
                        case MEDIUM:
                            difficult = ExtendedMode.MEDIUM;
                            break;
                        case HARD:
                            difficult = ExtendedMode.HARD;
                            break;
                        default:
                            break;
                    }
                }
                break;
            case SFIDA:
                basicMode = BasicMode.CHALLENGE;
                break;
            case ALLENAMENTO:
                basicMode = BasicMode.TRAINING;
                break;
        }

        switch (type){
            case CAPITALS:
                this.quiz = QuizFactory.createCapitalsQuiz(getOptionalOfSelectedMode());
                break;
            case MONUMENTS:
                this.quiz = QuizFactory.createMonumentsQuiz(getOptionalOfSelectedMode());
                break;
            case CURRENCIES:
                this.quiz = QuizFactory.createCurrenciesQuiz(getOptionalOfSelectedMode());
                break;
                case FLAGS:
                    this.quiz = QuizFactory.createFlagsQuiz(getOptionalOfSelectedMode());
            break;
            case DISHES:
                this.quiz = QuizFactory.createTypicalDishesQuiz(getOptionalOfSelectedMode());
            break;
            default:
                throw new IllegalArgumentException();
        }
    }

    private Optional getOptionalOfSelectedMode(){
        if(basicMode == null){
            if(difficult == null){
                return Optional.empty();
            } else {
                return Optional.of(difficult);
            }
        } else {
            return Optional.of(basicMode);
        }
    }

    @Override
    public String showStringQuestion() {
        return this.quiz.getCurrentQuestion().getQuestion();
    }

    @Override
    public InputStream showImageQuestion() {

        return ResourceLoader.loadResourceAsStream("/images/flags/" + this.quiz.getCurrentQuestion().getQuestion());

    }

    @Override
    public void hitAnswer(final Optional<String> answer) {
        this.quiz.hitAnswer(answer);

    }

    @Override
    public void nextQuestion() {
        if (!this.gameOver()) {
            this.quiz.next();
        }
    }

    @Override
    public boolean checkAnswer() {
        return this.quiz.isAnswerCorrect();
    }

    @Override
    public Set<String> getCorrectAnswers() {
        return this.quiz.getCorrectAnswers();
    }

    @Override
    public int getRemainingLives() {
        return this.quiz.getRemainingLives();
    }

    @Override
    public boolean gameOver() {
        return this.quiz.gameOver();
    }

    @Override
    public boolean isFreezeAvailable() {
        return this.quiz.isFreezeAvailable();
    }

    @Override
    public boolean isSkipAvailable() {
        return this.quiz.isSkipAvailable();
    }

    @Override
    public boolean is5050Available() {
        return this.quiz.is5050Available();
    }

    @Override
    public double freeze() {
        this.quiz.freeze();
        return QuizControllerImpl.FREEZE_TIME;
    }

    @Override
    public void skip() {
        this.quiz.skip();
    }

    @Override
    public Set<String> use5050() {
        return this.quiz.use5050();
    }

    @Override
    public int getQuestionDuration() {
        return this.quiz.getQuestionDuration();
    }

    @Override
    public void restart() {
        this.quiz.restart();
    }

    @Override
    public Set<String> showAnswers() {
        return this.quiz.getCurrentQuestion().getAnswers();
    }

    @Override
    public int getScore() {
        return this.quiz.getCurrentScore();
    }

    @Override
    public boolean everythingCorrect() {
        return quiz.allAnswersGiven();
    }

}
