package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.Map;

import com.geoquiz.utility.Pair;

import javafx.stage.Stage;

/**
 * The ranking scene where user can see other user's records.
 */
class AbsoluteRankingScene extends AbstractRankingScene {

    private static final String TITLE = "Global records";
    private Map<Pair<String, String>, Pair<String, Integer>> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public AbsoluteRankingScene(final Stage mainStage) {
        super(mainStage);

        getTitle().setText(TITLE);
    }

    @Override
    protected void initMap(){
        try {
            map = getRanking().getGlobalRanking();
        } catch (ClassNotFoundException | IOException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    protected String getRecordByCategory(final String category, final String difficulty) {
        final Pair<String, Integer> record = this.map.get(new Pair<>(category, difficulty));
        return record == null ? "" : record.getX() + " " + "(" + record.getY() + ")";
    }
}
