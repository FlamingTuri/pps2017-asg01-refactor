package com.geoquiz.model.question;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


/**
 * This class is the implementation for the Question interface.
 *
 */
class QuestionImpl implements Question {

    private final String question;
    private final Set<String> answers;
    private final Set<String> correctAnswers;
    private final Set<String> correctAnswersGiven = new HashSet<>();

    /**
     * @param question
     *          the question to be asked
     * @param answers
     *          the set of possible answers to be given
     */
    //package-private
    QuestionImpl(final String question, final Set<String> answers) {
        this.question = question;
        this.answers = answers;
        correctAnswers = new HashSet<>();
    }

    protected QuestionImpl(final String question, final Set<String> answers, final Set<String> correctAnswers) {
        this.question = question;
        this.answers = answers;
        this.correctAnswers = correctAnswers;
    }

    @Override
    public String getQuestion() {
        return this.question;
    }

    @Override
    public Set<String> getAnswers() {
        return Collections.unmodifiableSet(this.answers);
    }

    @Override
    public Set<String> getCorrectAnswers() {
        return Collections.unmodifiableSet(this.correctAnswers);
    }

    @Override
    public boolean isAnswerCorrect(final String answer) {
        Objects.requireNonNull(answer);
        if (!this.answers.contains(answer)) {
            throw new IllegalArgumentException("The given answer is not among the possible answers");
        }
        if (this.correctAnswers.contains(answer)) {
            correctAnswersGiven.add(answer);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean allAnswersGiven() {
        return correctAnswers.size() == correctAnswersGiven.size();
    }
}
