package com.geoquiz.view.label;

import javafx.scene.paint.Color;

/**
 * static factory for labels inside menu.
 */
public final class MyLabelBuilder {

    private String name = "";
    private Color color = Color.BLACK;
    private double font = 35;

    public MyLabelBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public MyLabelBuilder setColor(Color color) {
        this.color = color;
        return this;
    }

    public MyLabelBuilder setFont(double font) {
        this.font = font;
        return this;
    }

    public MyLabel build() {
        return new MyLabelImpl(name, color, font);
    }

}
