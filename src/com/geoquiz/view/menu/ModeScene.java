package com.geoquiz.view.menu;

import com.geoquiz.view.enums.Mode;
import com.geoquiz.view.button.MyButtonBuilder;
import com.geoquiz.view.utility.Background;

import javax.xml.bind.JAXBException;

import com.geoquiz.view.enums.ButtonsCategory;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelBuilder;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/**
 * The scene where user can choose game modality.
 */
class ModeScene extends GameScene {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 350;
    private static final double POS_3_Y = 200;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double LABEL_FONT = 35;
    private static final double OPACITY = 0.5;
    private static final double USER_LABEL_FONT = 40;
    private static final String DIRECTORY_SEPARATOR = "/";
    private static final String DEFAULT_LABEL_TEXT = "\nScegli prima la modalità di gioco!";
    private static final String IMAGE_DIRECTORY = "images";
    private static final String CAPITALS_TEXT = "Sai indicare la capitale di ciascun paese?";
    private static final String MONUMENTS_TEXT = "Sai indicare dove si trovano questi famosi monumenti?";
    private static final String FLAGS_TEXT = "Sai indicare i paesi in base alla bandiera nazionale?";
    private static final String DISHES_TEXT = "Sai indicare di quali paesi sono tipici questi piatti?";
    private static final String CURRENCIES_TEXT = "Sai indicare qual e' la valuta adottata da ciascun paese?";

    private final Stage mainStage;
    private final OptionsSelected optionsSelected;

    /**
     * Category
     *
     * @param mainStage the stage where the scene is called.
     */
    public ModeScene(final Stage mainStage, OptionsSelected optionsSelected) {
        super(mainStage);

        this.mainStage = mainStage;
        this.optionsSelected = optionsSelected;

        final MyButton back = new MyButtonBuilder().createMyButton();
        final MyButton classic = initializeModeButtons(Mode.CLASSICA);
        final MyButton challenge = initializeModeButtons(Mode.SFIDA);
        final MyButton training = initializeModeButtons(Mode.ALLENAMENTO);

        final MyLabel userLabel = new MyLabelBuilder().setName("USER: " + LoginMenuScene.getUsername())
                .setFont(USER_LABEL_FONT)
                .build();

        String labelText;
        switch (optionsSelected.getCategory()) {
            case CAPITALS:
                labelText = CAPITALS_TEXT;
                break;
            case MONUMENTS:
                labelText = MONUMENTS_TEXT;
                break;
            case FLAGS:
                labelText = FLAGS_TEXT;
                break;
            case DISHES:
                labelText = DISHES_TEXT;
                break;
            case CURRENCIES:
                labelText = CURRENCIES_TEXT;
                break;
            default:
                throw new IllegalArgumentException();
        }
        labelText += DEFAULT_LABEL_TEXT;
        Label label = new Label();
        label.setText(labelText);

        label.setFont(Font.font("Italic", FontWeight.BOLD, LABEL_FONT));

        VBox vbox = new VBox();
        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);
        vbox.getChildren().addAll((Node) classic, (Node) challenge, (Node) training);
        VBox vbox2 = new VBox();
        vbox2.getChildren().add((Node) back);
        VBox vbox3 = new VBox();
        vbox3.getChildren().add(label);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);
        vbox3.setTranslateX(POS_2_X);
        vbox3.setTranslateY(POS_3_Y);

        ((Node) back).setOnMouseClicked(event -> mainStage.setScene(new CategoryScene(mainStage, optionsSelected)));

        Pane panel = new Pane();
        panel.getChildren().addAll(ModeScene.createBackgroundImage(optionsSelected.getCategory()), Background.createBackground(),
                Background.getLogo(), vbox, vbox2, vbox3, (Node) userLabel);

        this.setRoot(panel);

    }

    private MyButton initializeModeButtons(Mode mode) {
        MyButton button = new MyButtonBuilder().setCategory(mode)
                .createMyButton();
        ((Node) button).setOnMouseClicked(event -> {
            optionsSelected.setMode((Mode) button.getCategory());
            if (mode.equals(Mode.CLASSICA) && (optionsSelected.getCategory().equals(ButtonsCategory.CAPITALS)
                    || optionsSelected.getCategory().equals(ButtonsCategory.MONUMENTS))) {
                mainStage.setScene(new LevelScene(mainStage, optionsSelected));
            } else {
                try {
                    mainStage.setScene(new QuizGamePlay(mainStage, optionsSelected));
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
        });
        return button;
    }

    /**
     * @return background image for own category.
     */
    public static ImageView createBackgroundImage(ButtonsCategory category) {
        final ImageView img;
        String imagePath = DIRECTORY_SEPARATOR + IMAGE_DIRECTORY + DIRECTORY_SEPARATOR;
        switch (category) {
            case CAPITALS:
                imagePath += "capitali.jpg";
                break;
            case MONUMENTS:
                imagePath += "monumenti.jpg";
                break;
            case CURRENCIES:
                imagePath += "valute.jpg";
                break;
            case DISHES:
                imagePath += "cucina.jpg";
                break;
            case FLAGS:
                imagePath += "bandiere.jpg";
                break;
            default:
                return Background.getImage();
        }
        img = Background.getCategoryImage(imagePath);
        img.setOpacity(OPACITY);
        return img;
    }

    public static ImageView createBackgroundImage() {
        return Background.getImage();
    }
}
