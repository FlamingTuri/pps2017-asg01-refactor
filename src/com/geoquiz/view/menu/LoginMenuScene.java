package com.geoquiz.view.menu;

import com.geoquiz.view.enums.LoginButtons;
import com.geoquiz.view.label.MyLabelBuilder;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.utility.ConfirmBox;
import com.geoquiz.view.utility.ExitProgram;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonBuilder;
import com.geoquiz.view.enums.Labels;
import com.geoquiz.view.label.MyLabel;

import java.io.IOException;

import com.geoquiz.controller.account.Account;
import com.geoquiz.controller.account.AccountImpl;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * The scene where user can choose difficulty level.
 */
class LoginMenuScene extends GameScene {

    private static final double TF_OPACITY = 0.7;
    private static final double TF_FONT = 25;
    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 525;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_3_X = 450;
    private static final double POS_3_Y = 300;

    private final TextField tfUser = new TextField();
    private final PasswordField tfPass = new PasswordField();
    private boolean okLogin;
    private static String username;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     * @throws IOException
     *             for exception login.
     */
    public LoginMenuScene(final Stage mainStage) throws IOException {
        super(mainStage);

        final MyLabel lblUser;
        final MyLabel lblPass;

        final MyButton btnEnter;
        final MyButton btnNewPlayer;
        final MyButton btnExit;

        final Account a = new AccountImpl("account.txt");

        btnExit = new MyButtonBuilder().setName(LoginButtons.EXIT.toString())
                                       .createMyButton();
        btnEnter = new MyButtonBuilder().setName(LoginButtons.LOGIN.toString())
                                        .createMyButton();
        btnNewPlayer = new MyButtonBuilder().setName(LoginButtons.REGISTER.toString())
                                            .createMyButton();

        lblUser = new MyLabelBuilder().setName(Labels.USERNAME.toString())
                                      .setColor(Color.WHITE)
                                      .build();
        lblPass = new MyLabelBuilder().setName(Labels.PASSWORD.toString())
                                      .setColor(Color.WHITE)
                                      .build();

        tfUser.getFont();
        tfUser.setFont(Font.font(TF_FONT));
        tfUser.setOpacity(TF_OPACITY);
        tfUser.setPromptText("Username");

        tfPass.getFont();
        tfPass.setFont(Font.font(TF_FONT));
        tfPass.setOpacity(TF_OPACITY);
        tfPass.setPromptText("Password");

        VBox vbox1 = new VBox();
        vbox1.setTranslateX(POS_1_X);
        vbox1.setTranslateY(POS_1_Y);
        VBox vbox2 = new VBox(20);
        vbox2.setTranslateX(POS_2_X);
        vbox2.setTranslateY(POS_2_Y);
        VBox vbox3 = new VBox(10);
        vbox3.setTranslateX(POS_3_X);
        vbox3.setTranslateY(POS_3_Y);

        vbox1.getChildren().addAll((Node) btnEnter, (Node) btnNewPlayer, (Node) btnExit);
        vbox2.getChildren().addAll((Node) lblUser, (Node) lblPass);
        vbox3.getChildren().addAll(tfUser, tfPass);

        ((Node) btnEnter).setOnMouseClicked(event -> {
            try {
                okLogin = true;
                a.checkLogin(getUser(), getPass());
            } catch (IllegalArgumentException e) {
                okLogin = false;
                final Alert alert = ConfirmBox.getAlert("Errore! Username o password errati!", Color.BLACK);
                alert.show();
                e.printStackTrace();
            }
            try {
                if (okLogin) {
                    username = tfUser.getText();
                    mainStage.setScene(new MainMenuScene(mainStage));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        ((Node) btnExit).setOnMouseClicked(event -> ExitProgram.exitProgram(mainStage));

        ((Node) btnNewPlayer).setOnMouseClicked(event -> {
            try {
                mainStage.setScene(new AccountRegisterScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox1, vbox2, vbox3,
                Background.getLogo());

        this.setRoot(panel);

    }

    /**
     * @return username autentication.
     */
    private String getUser() {
        return tfUser.getText();
    }

    /**
     * @return password autentication.
     */
    private String getPass() {
        return tfPass.getText();
    }

    /**
     * @return username.
     */
    public static String getUsername() {
        return username;
    }

}
