package com.geoquiz.view.button;

import com.geoquiz.view.enums.MenuButtons;
import javafx.scene.paint.Color;

/**
 * static factory for buttons inside menu.
 */
public final class MyButtonBuilder {

    private String name = MenuButtons.BACK.toString();
    private Enum category;
    private Color backgroundColor = Color.BLUE;
    private double width = 350;

    public MyButtonBuilder setName(String name){
        this.name = name;
        return this;
    }

    public MyButtonBuilder setCategory(Enum category) {
        this.category = category;
        return this;
    }

    public MyButtonBuilder setColor(Color backgroundColor){
        this.backgroundColor = backgroundColor;
        return this;
    }

    public MyButtonBuilder setWidth(double width){
        this.width = width;
        return this;
    }

    public MyButtonImpl createMyButton() {
        if(category == null){
            return new MyButtonImpl(name, backgroundColor, width);
        } else {
            return new MyButtonImpl(category, backgroundColor, width);
        }

    }
}
