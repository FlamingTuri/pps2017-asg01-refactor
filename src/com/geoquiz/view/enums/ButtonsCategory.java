package com.geoquiz.view.enums;

/**
 * The menu's buttons who a player can press to choose category and modality
 * game.
 */
public enum ButtonsCategory {
    /**
     * Represents the category "Capitals".
     */
    CAPITALS,
    /**
     * Represents the category "Currencies".
     */
    CURRENCIES,
    /**
     * Represents the category "Typical dishes".
     */
    DISHES,
    /**
     * Represents the category "Flags".
     */
    FLAGS,
    /**
     * Represents the category "Monuments".
     */
    MONUMENTS

}
