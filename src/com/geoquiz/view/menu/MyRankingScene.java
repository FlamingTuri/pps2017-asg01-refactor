package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.Map;

import com.geoquiz.utility.Pair;
import javafx.stage.Stage;

/**
 * The statistics scene where user can see own records.
 */
class MyRankingScene extends AbstractRankingScene {

    private static final String TITLE = "My records";
    private Map<Pair<String, String>, Integer> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public MyRankingScene(final Stage mainStage) {
        super(mainStage);

        getTitle().setText(TITLE);

    }

    @Override
    protected void initMap(){
        try {
            map = getRanking().getPersonalRanking(LoginMenuScene.getUsername());
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getRecordByCategory(final String category, final String difficulty) {
        final Integer record = this.map.get(new Pair<>(category, difficulty));
        return record == null ? "" : record.toString();
    }
}