package com.geoquiz.view.menu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonBuilder;
import com.geoquiz.view.utility.Background;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/**
 * The scene where user can read instructions game play.
 */
class InstructionScene extends GameScene {

    private static final double POS_X_INSTRUCTIONS = 100;
    private static final double POS_Y_INSTRUCTIONS = 160;
    private static final double INSTRUCTIONS_FONT = 23;
    private static final double POS_X_BACK_BOX = 900;
    private static final double POS_Y_BACK_BOX = 640;
    private static final String RESOURCE_FOLDER = "res";
    private static final String DATA_FOLDER = "data";
    private static final String FILENAME = "instructions.txt";

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public InstructionScene(final Stage mainStage) {
        super(mainStage);

        final MyButton back;
        back = new MyButtonBuilder().createMyButton();

        Label instructionsLabel = new Label();

        StringBuilder instructionsText = new StringBuilder();
        String instructionsPath = RESOURCE_FOLDER + File.separator + DATA_FOLDER + File.separator + FILENAME;
        try (BufferedReader br = new BufferedReader(new FileReader(instructionsPath))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                instructionsText.append(sCurrentLine).append("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        instructionsLabel.setText(instructionsText.toString());

        VBox instructionsBox = new VBox();
        instructionsBox.getChildren().add(instructionsLabel);
        instructionsLabel.setFont(Font.font("Italic", FontWeight.BOLD, INSTRUCTIONS_FONT));
        instructionsLabel.setTextFill(Color.BLACK);
        instructionsBox.setTranslateX(POS_X_INSTRUCTIONS);
        instructionsBox.setTranslateY(POS_Y_INSTRUCTIONS);
        VBox backBox = new VBox();
        backBox.setTranslateX(POS_X_BACK_BOX);
        backBox.setTranslateY(POS_Y_BACK_BOX);
        backBox.getChildren().add((Node) back);

        ((Node) back).setOnMouseClicked(event -> {
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(ModeScene.createBackgroundImage(), Background.createBackground(),
                Background.getLogo(), instructionsBox, backBox);

        this.setRoot(panel);
    }
}
