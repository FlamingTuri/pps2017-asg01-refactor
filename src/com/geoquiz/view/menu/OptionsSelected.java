package com.geoquiz.view.menu;

import com.geoquiz.view.enums.ButtonsCategory;
import com.geoquiz.view.enums.Difficulty;
import com.geoquiz.view.enums.MenuButtons;
import com.geoquiz.view.enums.Mode;

class OptionsSelected {

    private MenuButtons menuOption;
    private Mode mode;
    private ButtonsCategory category;
    private Difficulty difficulty;

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public ButtonsCategory getCategory() {
        return category;
    }

    public void setCategory(ButtonsCategory category) {
        this.category = category;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }
}
