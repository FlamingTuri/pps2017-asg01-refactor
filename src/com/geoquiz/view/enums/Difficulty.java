package com.geoquiz.view.enums;

public enum Difficulty {
    /**
     * Represents the difficulty level "Easy".
     */
    EASY,
    /**
     * Represents the difficulty level "Medium".
     */
    MEDIUM,
    /**
     * Represents the difficulty level "Hard".
     */
    HARD
}
